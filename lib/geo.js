// Подключаем модуль multi-geocoder
var MultiGeocoder = require('multi-geocoder'),
    // Получаем доступ к сервису геокодирования.  
    geocoder = new MultiGeocoder({
        coordorder: 'latlong',
        lang: 'ru-RU'
    });



// метод для получения геокоординат точки
var Geo = function(query) {
    return geocoder.geocode([
        query
    ], {
        // bbox: [
        //     [37.556586, 55.765554],
        //     [37.560682, 55.767863]
        // ]
    });
};




// *********************************

module.exports = Geo;