'use strict';

require('dotenv').config();

const Geo = require('./lib/geo');

const Hapi = require('hapi');
const Path = require('path');

const requestify = require('requestify');

const server = new Hapi.Server({});


server.connection({
    port: process.env.PORT
});

// ************************************************

server.register(require('inert'), (err) => {

    if (err) {
        throw err;
    }

    // получение yandex координат
    server.route({
        method: 'GET',
        path: '/addr/{name*}',
        handler: function(request, reply) {
            let addr = request.params.name;

            if (process.env.APP_DEBUG === 'true') {
                console.log(addr);
            }

            if (addr && addr.lenght > 3) {
                reply(`Не корректный запрос "${addr}" `).code(417);
            } else {
                Geo(addr)
                    .then((data) => {
                        reply(data);
                    });
            }
        }
    });

    // список точек котрые надо просмотреть
    server.route({
        method: 'GET',
        path: '/points',
        handler: function(request, reply) {
            requestify.get(process.env.ADDR_LIST)
                .then((resp) => {
                    reply(resp.getBody());
                });
        }
    });

    // статические файлы
    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'public',
                listing: true
            }
        }
    });




    // server.route({
    //     method: 'GET',
    //     path: '/',
    //     handler: function(request, reply) {
    //         reply('Hello, world!');
    //     }
    // });




    // *******************************************

    server.start((err) => {

        if (err) {
            throw err;
        }
        console.log('Server running at:', server.info.uri);
    });

});