console.log('%cProject is running', "font-size:2em; color:#4e9cf3;");

var app = angular.module('app', []);

app.controller('MainCtrl', ['$scope', '$http', function($scope, $http) {

    // тут начались карты

    ymaps.ready(init);

    function init() {
        var map = new ymaps.Map('YMapsID', {
            center: [55.76, 37.64],
            zoom: 7
        });


        var add_placemark = function(coord, desc) {
            if (coord === undefined) {
                return false;
            }

            var pos;

            if (typeof coord === 'string') {
                pos = coord.split(' ').reverse();
            } else {
                pos = coord;
            }

            placemark = new ymaps.Placemark([pos[0], pos[1]], {
                balloonContent: desc,
                hintContent: desc
            });

            placemark.description = desc || '--';
            map.geoObjects.add(placemark);
        };

        var getProp = function(obj, prop_path, def) {
            if (obj === null || obj === undefined) {
                return def;
            }

            var arr = prop_path.split('.'),
                prop,
                i;

            for (i = 0; i < arr.length; i++) {
                prop = arr[i];
                if (obj.hasOwnProperty(prop) && obj[prop] !== undefined) {
                    obj = obj[prop];
                } else {
                    return def;
                }
                if (obj === null || obj === undefined) {
                    return def;
                }
            }
            return obj;
        };


        /**
         * получение свойств из объекта
         * @param  {object} obj   объект который проверяем
         * @param  {array|string} arr_keys список свойств которые нужно найти в объекте
         * @param  {object} res   объект куда собираем значения
         * @param  {boolean} single если TRUE то возвращаем одно значение
         * @return {object}
         */
        var getObjVal = function(obj, arr_keys, res, single) {
            var k, v,
                hasProp = {}.hasOwnProperty;

            if (typeof res !== 'object') {
                res = {};
            }
            if (typeof arr_keys === 'string') {
                arr_keys = [arr_keys];
            }

            for (k in obj) {
                if (!hasProp.call(obj, k)) continue;
                v = obj[k];

                if (v && arr_keys.indexOf(k) > -1) {
                    res[k] = v;
                    continue;
                }

                if (typeof v === 'object' && v !== null) {
                    getObjVal(v, arr_keys, res);
                }
            }

            return single ? res[arr_keys[0]] : res;
        };

        // ****************************************************************


        $http
        // .get('http://www.onlime.ru/connection/get_addrlist.php')
            .get('/points')
            .then(function(res) {
                return res.data.split('\n');
            })
            .then(function(arr) {
                arr.shift();


                var addr = arr.map(function(item) {
                    return [
                        item.replace(/^(\d*).+"/, '$1'),
                        item.replace(/.*("[^"]+")?.+"([^"]+)"/, '$2')
                    ].join(', ');
                }).filter(function(val) {
                    return !!val;
                });
                console.table(addr);

                return addr;
            })
            .then(function(addr) {
                addr.map(function(item) {
                    /*
                    // проходим по строкам и ставим точки
                    // var url = [
                    //     'https://geocode-maps.yandex.ru/1.x/?format=json',
                    //     'geocode=' + item.replace(/\s/g, '+'),
                    //     'll=37.618920,55.756994',
                    //     'spn=3.552069,2.40055'
                    // ].join('&'); 

                    $http
                        .get(url)
                        .then(function(data) {
                            var res = {
                                geoObj: getProp(data, 'data.response.GeoObjectCollection.featureMember', false),
                                request: getProp(data, 'data.response.GeoObjectCollection.metaDataProperty.GeocoderResponseMetaData.request', false)
                            };

                            if (!res.geoObj || !res.request) {
                                throw "ошибка ответа";
                            }

                            return res;
                        })
                        .then(function(data) {
                            var p = getObjVal(data.geoObj, 'pos', {}, true);
                            add_placemark(p, data.request);
                        })
                        .catch(function(err) {
                            console.warn(err);
                        });
                    // */
                    if (item === ', ') {
                        return false;
                    }

                    var url = '/addr/' + item;
                    $http
                        .get(url)
                        .then(function(data) {
                            var coordinates = getObjVal(data, 'coordinates', {}, true);

                            add_placemark(coordinates, item);
                        })
                        .catch(function(err) {
                            console.warn(err);
                        });
                });
            })
            .catch(function(err) {
                console.warn(err);
            });
    }
    // тут кончились карты
}]);